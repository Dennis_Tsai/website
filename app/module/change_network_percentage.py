import sys,time
import matplotlib
matplotlib.use('Agg')
import os, pandas as pd, multiprocessing as mp, numpy as np, networkx as nx, operator, ranking
import matplotlib.font_manager as fm, matplotlib.pyplot as plt, seaborn as sns
from datetime import datetime
from itertools import product, combinations
from scipy.stats import spearmanr
from collections import Counter
from scipy.stats import linregress
from fisher import pvalue

def to_subtype(rna, file_path, sample_list):
    fname = '_'.join([file_path, rna, 'expression_profile.txt'])
    data = pd.read_table(fname, sep='\t', header=0, index_col=0, encoding='utf-8')
    for key, val in sample_list.items():
        temp = data.loc[:, val]
        fname = '_'.join([file_path, 'subtype-'+str(key), rna, 'expression_profile.txt'])
        temp.to_csv(fname, sep='\t', encoding='utf-8')
        del temp
    del data
    return 0

def cancer_subtype(processes, folder_path, case_id, n_subtype):
    pool = mp.Pool(processes=processes)
    results = []
    subtype_samples = {}
    for subtype_idx in range(1, n_subtype+1):
        fname = '_'.join([case_id, 'subtype', str(subtype_idx)])+'.txt'
        samples = pd.read_table('/'.join([folder_path, 'subtype', fname]), sep='\t', header=None, index_col=None, encoding='utf-8')
        subtype_samples.update({subtype_idx:sorted(sum(samples.values.tolist(), []))})
        del samples
    for rna in ['mRNA', 'lncRNA', 'miRNA']:
        results.append(pool.apply_async(to_subtype, args=(rna, folder_path+'/'+case_id, subtype_samples)))
    results = [p.get() for p in results]

def coherent_association(xtype, ytype, ztype, file_path, cutoff, uniProt_idmapping_selected):
    fname = '_'.join([file_path, 'association_index.txt'])
    data = pd.read_table(fname, sep='\t', header=0, index_col=[0, 1], encoding='utf-8')
    n = int(np.ceil(data.shape[0]*(cutoff/100)))
    pairs_counter = Counter()
    for method in ['Jaccard', 'Simpson', 'Geometric', 'Cosine', 'PCC']:
        pairs_counter += Counter(data.sort_values(by=method, ascending=False).index.values.tolist()[:n])
    G = nx.Graph()
    G.add_edges_from([key for key, val in pairs_counter.items() if val > 2])
    del data, n, pairs_counter
    module = [(g, g.number_of_nodes()) for g in nx.connected_component_subgraphs(G) if g.number_of_nodes() > 2]
    color_code = sns.color_palette('Set3', n_colors=len(module)).as_hex()
    fname = '2'.join([file_path, 'mRNA_bipartite_coexpression_network.txt'])
    data = pd.read_table(fname, sep='\t', header=0, encoding='utf-8')
    lncRNA2mRNA = nx.from_pandas_edgelist(data, source='lncRNA_ID', target='mRNA_ID')
    del data
    fname1 = '_'.join([file_path, 'association_network_modified.txt'])
    fname2 = '_'.join([file_path, 'association_network_nodes_color_modified.txt'])
    with open(fname1, mode='w', encoding='utf-8') as fw1, open(fname2, mode='w', encoding='utf-8') as fw2:
        for idx, (m, c) in enumerate(sorted(module, key=lambda x: x[1], reverse=True)):
            temp = ['\t'.join(e) for e in m.edges()]
            fw1.write('%s\n'%('\n'.join(temp)))
            fname = '_'.join([file_path, 'association_network_module'+str(idx)+'_modified.txt'])
            with open(fname, mode='w', encoding='utf-8') as fw:
                fw.write('%s\n'%('\n'.join(temp)))
            temp = ['\t'.join([n, color_code[idx]]) for n in m.nodes()]
            fw2.write('%s\n'%('\n'.join(temp)))
            del temp
            mRNA_list = []
            for n in m.nodes():
                mRNA_list.extend(lncRNA2mRNA.neighbors(n))
            mRNA_list = list(set(mRNA_list))
            uniprotkb_ac = []
            for mRNA in mRNA_list:
                if mRNA in uniProt_idmapping_selected:
                    uniprotkb_ac.extend(uniProt_idmapping_selected.get(mRNA))
            del mRNA_list
            uniprotkb_ac = list(set(uniprotkb_ac))
            fname = '_'.join([file_path, 'association_network_module'+str(idx)+'_mRNA_list_modified.txt'])
            with open(fname, mode='w', encoding='utf-8') as fw:
                fw.write('%s\n'%('\n'.join(uniprotkb_ac)))
            del uniprotkb_ac
    return len(module)

def identify_coherent_association(processes, folder_path, case_id, n_subtype, cutoff, uniProt_idmapping_file_path):
    pool = mp.Pool(processes=processes)
    results = []
    xtype, ytype, ztype = 'lncRNA', 'mRNA', 'miRNA'
    uniProt_idmapping_selected = {}
    with open(uniProt_idmapping_file_path, 'r', encoding='utf-8') as fr:
        for line in fr:
            items = line.strip().split('\t')
            Ensembl = []
            for item in items[2:]:
                if 'ENSG' in item:
                    Ensembl.extend(item.split('; '))
            for gene in Ensembl:
                if gene not in uniProt_idmapping_selected:
                    uniProt_idmapping_selected.update({gene:[]})
                uniProt_idmapping_selected[gene].append(items[0])
            Ensembl = None
    if n_subtype > 0:
        for subtype_idx in range(1, n_subtype+1):
            file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), xtype])])
            results.append([subtype_idx, pool.apply_async(coherent_association, args=(xtype, ytype, ztype, file_path, cutoff, uniProt_idmapping_selected))])
    else:
        file_path = '/'.join([folder_path, '_'.join([case_id, xtype])])
        results.append([0, pool.apply_async(coherent_association, args=(xtype, ytype, ztype, file_path, cutoff, uniProt_idmapping_selected))])
    results = [[p[0], p[1].get()] for p in results]
    return results
def main(cmdArgument):
    CASE_ID = cmdArgument[1]
    PATH = cmdArgument[2]
    cutoff = cmdArgument[3]
    UNIPROT_IDMAPPING_FILE_PATH = '/home/dennistsai/uploads/HUMAN_9606_idmapping_selected.tab'
    GENE_ONTOLOGY_FILE_PATH = '/home/dennistsai/uploads/Gene_Ontology_BiologicalProcess_UniProt-GOA-experimental-evidence-code_20180320.txt'
    GENE_ONTOLOGY_RELATIONS_FILE_PATH = '/home/dennistsai/uploads/go-basic_20180318_tree.txt'
    Num_of_PROCESSES = 3
    if os.path.exists('/'.join([PATH, 'subtype'])):
        Num_of_SUBTYPE = len(os.listdir('/'.join([PATH, 'subtype'])))
        cancer_subtype(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE)
    else:
        Num_of_SUBTYPE = 0
    print(CASE_ID + '*' + PATH)
    print(float(cutoff))
    Num_of_MODULES = identify_coherent_association(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE, float(cutoff), UNIPROT_IDMAPPING_FILE_PATH)

if __name__ == '__main__':
    main(sys.argv)

