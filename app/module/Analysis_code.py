# -*- coding: utf-8 -*-
"""
Created on Fri Mar 30 06:13:33 2018

@author: user
"""

import sys,time
import matplotlib
matplotlib.use('Agg')
import os, pandas as pd, multiprocessing as mp, numpy as np, networkx as nx, operator, ranking
import matplotlib.font_manager as fm, matplotlib.pyplot as plt, seaborn as sns
from datetime import datetime
from itertools import product, combinations
from scipy.stats import spearmanr
from collections import Counter
from scipy.stats import linregress
from fisher import pvalue

def to_subtype(rna, file_path, sample_list):
    fname = '_'.join([file_path, rna, 'expression_profile.txt'])
    data = pd.read_table(fname, sep='\t', header=0, index_col=0, encoding='utf-8')
    for key, val in sample_list.items():
        temp = data.loc[:, val]
        fname = '_'.join([file_path, 'subtype-'+str(key), rna, 'expression_profile.txt'])
        temp.to_csv(fname, sep='\t', encoding='utf-8')
        del temp
    del data
    return 0
    
def cancer_subtype(processes, folder_path, case_id, n_subtype):
    pool = mp.Pool(processes=processes)
    results = []
    subtype_samples = {}
    for subtype_idx in range(1, n_subtype+1):
        fname = '_'.join([case_id, 'subtype', str(subtype_idx)])+'.txt'
        samples = pd.read_table('/'.join([folder_path, 'subtype', fname]), sep='\t', header=None, index_col=None, encoding='utf-8')
        subtype_samples.update({subtype_idx:sorted(sum(samples.values.tolist(), []))})
        del samples
    for rna in ['mRNA', 'lncRNA', 'miRNA']:
        results.append(pool.apply_async(to_subtype, args=(rna, folder_path+'/'+case_id, subtype_samples)))
    results = [p.get() for p in results]

def correlation(rna, fname_x, fname_y, fname_id, fname_corr, cutoff):
    expression_lncRNA = pd.read_table(fname_x, sep='\t', header=0, index_col=0, encoding='utf-8')
    Median = expression_lncRNA.quantile(q=0.5, axis='columns')
    expression_lncRNA = expression_lncRNA.loc[Median.index.values[np.where(Median > cutoff)[0]].tolist(), :]
    del Median
    expression = pd.read_table(fname_y, sep='\t', header=0, index_col=0, encoding='utf-8')
    if 'mRNA' in rna:
        Median = expression.quantile(q=0.5, axis='columns')
        expression = expression.loc[Median.index.values[np.where(Median > cutoff)[0]].tolist(), :]
        del Median
    data = pd.concat([expression_lncRNA, expression], axis='index', keys=['lncRNA', rna], names=['group_key', 'ID'])
    del expression_lncRNA, expression
    data_index = data.index.get_level_values('ID').tolist()
    n = data.shape[1]
    idx = data.index.get_level_values('group_key') == 'lncRNA'
    lncRNA_idx = idx.nonzero()[0]
    idx = data.index.get_level_values('group_key') == rna
    rna_idx = idx.nonzero()[0]
    del idx
    pairs_idx = pd.DataFrame([[x, y] for x, y in product(lncRNA_idx, rna_idx)], columns=['lncRNA_idx', '_'.join([rna, 'idx'])])
    pairs_ID = pd.DataFrame([[data_index[x], data_index[y]] for x, y in product(lncRNA_idx, rna_idx)], columns=['lncRNA_ID', '_'.join([rna, 'ID'])])
    del data_index, lncRNA_idx, rna_idx
    pairs_ID.to_csv(fname_id, sep='\t', index=False, encoding='utf-8')
    del pairs_ID
    corr = spearmanr(data, axis=1)[0]
    del data
    rho = pd.DataFrame(corr[pairs_idx['lncRNA_idx'], pairs_idx['_'.join([rna, 'idx'])]], columns=['SCC'])
    del corr
    rho['z-score'] = np.sqrt((n-3)/1.06)*(0.5*np.log((1+rho['SCC'])/(1-rho['SCC'])))
    rho.to_csv(fname_corr, sep='\t', na_rep='NA', index=False, encoding='utf-8')
    del n, pairs_idx, rho
    return 0

def calculate_spearman_rank_order_correlation_coefficient(processes, folder_path, case_id, n_subtype, cutoff):
    pool = mp.Pool(processes=processes)
    results = []
    if n_subtype > 0:
        for subtype_idx in range(1, n_subtype+1):
            for rna in ['mRNA', 'miRNA']:
                x_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), 'lncRNA_expression_profile.txt'])])
                y_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), rna, 'expression_profile.txt'])])
                id_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join(['lncRNA', rna]), 'ID.txt'])])
                corr_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join(['lncRNA', rna]), 'correlation.txt'])])
                results.append(pool.apply_async(correlation, args=(rna, x_fname, y_fname, id_fname, corr_fname, cutoff)))
    else:
        for rna in ['mRNA', 'miRNA']:
            x_fname = '/'.join([folder_path, '_'.join([case_id, 'lncRNA_expression_profile.txt'])])
            y_fname = '/'.join([folder_path, '_'.join([case_id, rna, 'expression_profile.txt'])])
            id_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join(['lncRNA', rna]), 'ID.txt'])])
            corr_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join(['lncRNA', rna]), 'correlation.txt'])])
            results.append(pool.apply_async(correlation, args=(rna, x_fname, y_fname, id_fname, corr_fname, cutoff)))
    results = [p.get() for p in results]

def bipartite_network(fname_id, fname_corr, fname_network):
    data = pd.read_table(fname_corr, sep='\t', header=0, index_col=None, usecols=['z-score'], encoding='utf-8')
    id_list = pd.read_table(fname_id, sep='\t', header=0, index_col=None, encoding='utf-8')
    for threshold in np.arange(2, 10.1, 0.25):
        temp = id_list.loc[data.index.values[np.where(data['z-score'] > threshold)[0]].tolist(), :]
        G = nx.from_pandas_edgelist(temp, source=temp.columns[0], target=temp.columns[1])
        del temp
        n = G.number_of_nodes()
        degree, probability = [], []
        degree_list = [G.degree(i) for i in G.nodes()]
        for key, val in Counter(degree_list).items():
            degree.append(np.log10(key))
            probability.append(np.log10(val/n))
        r_squared = linregress(degree, probability)[2]**2
        del G, n, degree, degree_list, probability
        if r_squared > 0.8:
            break
    temp = id_list.loc[data.index.values[np.where(data['z-score'] > threshold)[0]].tolist(), :]
    del data, id_list
    temp.to_csv(fname_network, sep='\t', na_rep='NA', index=False, encoding='utf-8')
    del temp
    return 0

def construct_bipartite_coexpression_network(processes, folder_path, case_id, n_subtype):
    pool = mp.Pool(processes=processes)
    results = []
    if n_subtype > 0:
        for subtype_idx in range(1, n_subtype+1):
            for rna in ['mRNA', 'miRNA']:
                id_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join(['lncRNA', rna]), 'ID.txt'])])
                corr_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join(['lncRNA', rna]), 'correlation.txt'])])
                network_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join(['lncRNA', rna]), 'bipartite_coexpression_network.txt'])])
                results.append(pool.apply_async(bipartite_network, args=(id_fname, corr_fname, network_fname)))
    else:
        for rna in ['mRNA', 'miRNA']:
            id_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join(['lncRNA', rna]), 'ID.txt'])])
            corr_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join(['lncRNA', rna]), 'correlation.txt'])])
            network_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join(['lncRNA', rna]), 'bipartite_coexpression_network.txt'])])
            results.append(pool.apply_async(bipartite_network, args=(id_fname, corr_fname, network_fname)))
    results = [p.get() for p in results]

def ai(setA, setB, Ny):
    NabIntersection = np.abs(len(setA&setB))
    NabUnion = np.abs(len(setA|setB))
    Na = np.abs(len(setA))
    Nb = np.abs(len(setB))
    minab = min(Na,Nb)
    Jaccard = NabIntersection/NabUnion
    Simpson = NabIntersection/minab
    Geometric = (NabIntersection**2)/(Na*Nb)
    Cosine = NabIntersection/((Na*Nb)**0.5)
    PCC = ((NabIntersection*Ny)-(Na*Nb))/((Na*Nb*(Ny-Na)*(Ny-Nb))**0.5)
    s = '-'
    if PCC >= 0:
        s = 'p'
    else:
        s = 'n'
    return {'Jaccard':Jaccard, 'Simpson':Simpson, 'Geometric':Geometric, 'Cosine':Cosine, 'PCC':np.abs(PCC), 'positive/negative':s}

def association_index(xtype, ytype, ztype, fname_xy, fname_xz, file_path, file_path_y, file_path_z):
    interaction = {}
    network = pd.read_table(fname_xy, sep='\t', header=0, index_col=None, encoding='utf-8')
    temp = []
    for [X, Y] in network.loc[:, [xtype+'_ID', ytype+'_ID']].values:
        if X not in interaction:
            interaction.update({X:{ytype:[], ztype:[]}})
        interaction[X][ytype].append(Y)
        temp.append(Y)
    total_ytype_nodes = len(set(temp))
    del network, temp, X, Y
    network = pd.read_table(fname_xz, sep='\t', header=0, index_col=None, encoding='utf-8')
    temp = []
    for [X, Z] in network.loc[:, [xtype+'_ID', ztype+'_ID']].values:
        if X not in interaction:
            interaction.update({X:{ytype:[], ztype:[]}})
        interaction[X][ztype].append(Z)
        temp.append(Z)
    total_ztype_nodes = len(set(temp))
    del network, temp, X, Z
    key_list = list(interaction.keys())
    for key in key_list:
        if (len(interaction[key][ytype])==0) or (len(interaction[key][ztype])==0):
            interaction.pop(key)
    del key_list, key
    fname_Jaccard_y = '_'.join([file_path_y, 'association_index_Jaccard.txt'])
    fname_Simpson_y = '_'.join([file_path_y, 'association_index_Simpson.txt'])
    fname_Geometric_y = '_'.join([file_path_y, 'association_index_Geometric.txt'])
    fname_Cosine_y = '_'.join([file_path_y, 'association_index_Cosine.txt'])
    fname_PCC_y = '_'.join([file_path_y, 'association_index_PCC.txt'])
    fname_Jaccard_z = '_'.join([file_path_z, 'association_index_Jaccard.txt'])
    fname_Simpson_z = '_'.join([file_path_z, 'association_index_Simpson.txt'])
    fname_Geometric_z = '_'.join([file_path_z, 'association_index_Geometric.txt'])
    fname_Cosine_z = '_'.join([file_path_z, 'association_index_Cosine.txt'])
    fname_PCC_z = '_'.join([file_path_z, 'association_index_PCC.txt'])
    fname = '_'.join([file_path, 'association_index.txt'])
    with open(fname_Jaccard_y, mode='w', encoding='utf-8') as fw_Jaccard_y, open(fname_Simpson_y, mode='w', encoding='utf-8') as fw_Simpson_y, open(fname_Geometric_y, mode='w', encoding='utf-8') as fw_Geometric_y, open(fname_Cosine_y, mode='w', encoding='utf-8') as fw_Cosine_y, open(fname_PCC_y, mode='w', encoding='utf-8') as fw_PCC_y, open(fname_Jaccard_z, mode='w', encoding='utf-8') as fw_Jaccard_z, open(fname_Simpson_z, mode='w', encoding='utf-8') as fw_Simpson_z, open(fname_Geometric_z, mode='w', encoding='utf-8') as fw_Geometric_z, open(fname_Cosine_z, mode='w', encoding='utf-8') as fw_Cosine_z, open(fname_PCC_z, mode='w', encoding='utf-8') as fw_PCC_z, open(fname, mode='w', encoding='utf-8') as fw:
        fw_Jaccard_y.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Jaccard'))
        fw_Simpson_y.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Simpson'))
        fw_Geometric_y.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Geometric'))
        fw_Cosine_y.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Cosine'))
        fw_PCC_y.write('%s\t%s\t%s\t%s\n'%('A_name', 'B_name', 'PCC', 'positive/negative'))
        fw_Jaccard_z.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Jaccard'))
        fw_Simpson_z.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Simpson'))
        fw_Geometric_z.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Geometric'))
        fw_Cosine_z.write('%s\t%s\t%s\n'%('A_name', 'B_name', 'Cosine'))
        fw_PCC_z.write('%s\t%s\t%s\t%s\n'%('A_name', 'B_name', 'PCC', 'positive/negative'))
        fw.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%('A_name', 'B_name', 'Jaccard', 'Simpson', 'Geometric', 'Cosine', 'PCC'))
        for (A_name, B_name) in combinations(list(interaction.keys()), 2):
            ai_val_y = ai(set(interaction[A_name][ytype]), set(interaction[B_name][ytype]), total_ytype_nodes)
            fw_Jaccard_y.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_y.get('Jaccard')))
            fw_Simpson_y.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_y.get('Simpson')))
            fw_Geometric_y.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_y.get('Geometric')))
            fw_Cosine_y.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_y.get('Cosine')))
            fw_PCC_y.write('%s\t%s\t%f\t%s\n'%(A_name, B_name, ai_val_y.get('PCC'), ai_val_y.get('positive/negative')))
            ai_val_z = ai(set(interaction[A_name][ztype]), set(interaction[B_name][ztype]), total_ztype_nodes)
            fw_Jaccard_z.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_z.get('Jaccard')))
            fw_Simpson_z.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_z.get('Simpson')))
            fw_Geometric_z.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_z.get('Geometric')))
            fw_Cosine_z.write('%s\t%s\t%f\n'%(A_name, B_name, ai_val_z.get('Cosine')))
            fw_PCC_z.write('%s\t%s\t%f\t%s\n'%(A_name, B_name, ai_val_z.get('PCC'), ai_val_z.get('positive/negative')))
            fw.write('%s\t%s\t%f\t%f\t%f\t%f\t%f\n'%(A_name, B_name, ai_val_y.get('Jaccard')+ai_val_z.get('Jaccard'), 
                                                     ai_val_y.get('Simpson')+ai_val_z.get('Simpson'), 
                                                     ai_val_y.get('Geometric')+ai_val_z.get('Geometric'), 
                                                     ai_val_y.get('Cosine')+ai_val_z.get('Cosine'), 
                                                     ai_val_y.get('PCC')+ai_val_z.get('PCC')))
            del ai_val_y, ai_val_z
    BINS = 10
    for method in ['Jaccard', 'Simpson', 'Geometric', 'Cosine', 'PCC']:
        hist1 = {}
        fname = '_'.join([file_path_y, 'association_index', method])+'.txt'
        data = pd.read_table(fname, sep='\t', header=0, index_col=None, encoding='utf-8')
        data = data.loc[:, method]
        data = data.iloc[list(np.nonzero(-np.isnan(data))[0])]
        N = data.shape[0]
        bin_size = [int(np.fix(N/BINS)) for idx in range(BINS)]
        bin_size[BINS-1] = int(np.fix(N/BINS)) + int(N-np.sum(bin_size))
        argsort_data = np.argsort(data)
        del data
        idx = 0
        for bin_idx in range(BINS):
            for flag in range(bin_size[bin_idx]):
                hist1.update({argsort_data.iloc[idx]:bin_idx})
                idx += 1
        del bin_size, argsort_data
        hist2 = {}
        fname = '_'.join([file_path_z, 'association_index', method])+'.txt'
        data = pd.read_table(fname, sep='\t', header=0, index_col=None, encoding='utf-8')
        data = data.loc[:, method]
        data = data.iloc[list(np.nonzero(-np.isnan(data))[0])]
        N = data.shape[0]
        bin_size = [int(np.fix(N/BINS)) for idx in range(BINS)]
        bin_size[BINS-1] = int(np.fix(N/BINS)) + int(N-np.sum(bin_size))
        argsort_data = np.argsort(data)
        del data
        idx = 0
        for bin_idx in range(BINS):
            for flag in range(bin_size[bin_idx]):
                hist2.update({argsort_data.iloc[idx]:bin_idx})
                idx += 1
        del bin_size, argsort_data
        M = pd.DataFrame([list(map(int, np.zeros(BINS))) for idx in range(BINS)])
        for idx in hist1.keys():
            x_bin_idx = hist1[idx]
            y_bin_idx = hist2[idx]
            M.iloc[y_bin_idx, x_bin_idx] += 1
        del hist1, hist2
        idx = sorted(range(M.shape[0]), reverse=True)
        M = M.iloc[idx,:]
        del idx
        fname = '_'.join([file_path, 'association_index_histmatrix', method])+'.txt'
        M.to_csv(fname, sep='\t', header=False, index=False, encoding='utf-8')
    return 0

def calculate_association_index(processes, folder_path, case_id, n_subtype):
    pool = mp.Pool(processes=processes)
    results = []
    xtype, ytype, ztype = 'lncRNA', 'mRNA', 'miRNA'
    if n_subtype > 0:
        for subtype_idx in range(1, n_subtype+1):
            xy_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join([xtype, ytype]), 'bipartite_coexpression_network.txt'])])
            xz_fname = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join([xtype, ztype]), 'bipartite_coexpression_network.txt'])])
            file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), xtype])])
            y_file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join([xtype, ytype])])])
            z_file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), '2'.join([xtype, ztype])])])
            results.append(pool.apply_async(association_index, args=(xtype, ytype, ztype, xy_fname, xz_fname, file_path, y_file_path, z_file_path)))
    else:
        xy_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join([xtype, ytype]), 'bipartite_coexpression_network.txt'])])
        xz_fname = '/'.join([folder_path, '_'.join([case_id, '2'.join([xtype, ztype]), 'bipartite_coexpression_network.txt'])])
        file_path = '/'.join([folder_path, '_'.join([case_id, xtype])])
        y_file_path = '/'.join([folder_path, '_'.join([case_id, '2'.join([xtype, ytype])])])
        z_file_path = '/'.join([folder_path, '_'.join([case_id, '2'.join([xtype, ztype])])])
        results.append(pool.apply_async(association_index, args=(xtype, ytype, ztype, xy_fname, xz_fname, file_path, y_file_path, z_file_path)))
    results = [p.get() for p in results]

def coherent_association(xtype, ytype, ztype, file_path, cutoff, uniProt_idmapping_selected):
    fname = '_'.join([file_path, 'association_index.txt'])
    data = pd.read_table(fname, sep='\t', header=0, index_col=[0, 1], encoding='utf-8')
    n = int(np.ceil(data.shape[0]*(cutoff/100)))
    pairs_counter = Counter()
    for method in ['Jaccard', 'Simpson', 'Geometric', 'Cosine', 'PCC']:
        pairs_counter += Counter(data.sort_values(by=method, ascending=False).index.values.tolist()[:n])
    G = nx.Graph()
    G.add_edges_from([key for key, val in pairs_counter.items() if val > 2])
    del data, n, pairs_counter
    module = [(g, g.number_of_nodes()) for g in nx.connected_component_subgraphs(G) if g.number_of_nodes() > 2]
    color_code = sns.color_palette('Set3', n_colors=len(module)).as_hex()
    fname = '2'.join([file_path, 'mRNA_bipartite_coexpression_network.txt'])
    data = pd.read_table(fname, sep='\t', header=0, encoding='utf-8')
    lncRNA2mRNA = nx.from_pandas_edgelist(data, source='lncRNA_ID', target='mRNA_ID')
    del data
    fname1 = '_'.join([file_path, 'association_network.txt'])
    fname2 = '_'.join([file_path, 'association_network_nodes_color.txt'])
    with open(fname1, mode='w', encoding='utf-8') as fw1, open(fname2, mode='w', encoding='utf-8') as fw2:
        for idx, (m, c) in enumerate(sorted(module, key=lambda x: x[1], reverse=True)):
            temp = ['\t'.join(e) for e in m.edges()]
            fw1.write('%s\n'%('\n'.join(temp)))
            fname = '_'.join([file_path, 'association_network_module'+str(idx)+'.txt'])
            with open(fname, mode='w', encoding='utf-8') as fw:
                fw.write('%s\n'%('\n'.join(temp)))
            temp = ['\t'.join([n, color_code[idx]]) for n in m.nodes()]
            fw2.write('%s\n'%('\n'.join(temp)))
            del temp
            mRNA_list = []
            for n in m.nodes():
                mRNA_list.extend(lncRNA2mRNA.neighbors(n))
            mRNA_list = list(set(mRNA_list))
            uniprotkb_ac = []
            for mRNA in mRNA_list:
                if mRNA in uniProt_idmapping_selected:
                    uniprotkb_ac.extend(uniProt_idmapping_selected.get(mRNA))
            del mRNA_list
            uniprotkb_ac = list(set(uniprotkb_ac))
            fname = '_'.join([file_path, 'association_network_module'+str(idx)+'_mRNA_list.txt'])
            with open(fname, mode='w', encoding='utf-8') as fw:
                fw.write('%s\n'%('\n'.join(uniprotkb_ac)))
            del uniprotkb_ac
    return len(module)

def identify_coherent_association(processes, folder_path, case_id, n_subtype, cutoff, uniProt_idmapping_file_path):
    pool = mp.Pool(processes=processes)
    results = []
    xtype, ytype, ztype = 'lncRNA', 'mRNA', 'miRNA'
    uniProt_idmapping_selected = {}
    with open(uniProt_idmapping_file_path, 'r', encoding='utf-8') as fr:
        for line in fr:
            items = line.strip().split('\t')
            Ensembl = []
            for item in items[2:]:
                if 'ENSG' in item:
                    Ensembl.extend(item.split('; '))
            for gene in Ensembl:
                if gene not in uniProt_idmapping_selected:
                    uniProt_idmapping_selected.update({gene:[]})
                uniProt_idmapping_selected[gene].append(items[0])
            Ensembl = None
    if n_subtype > 0:
        for subtype_idx in range(1, n_subtype+1):
            file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), xtype])])
            results.append([subtype_idx, pool.apply_async(coherent_association, args=(xtype, ytype, ztype, file_path, cutoff, uniProt_idmapping_selected))])
    else:
        file_path = '/'.join([folder_path, '_'.join([case_id, xtype])])
        results.append([0, pool.apply_async(coherent_association, args=(xtype, ytype, ztype, file_path, cutoff, uniProt_idmapping_selected))])
    results = [[p[0], p[1].get()] for p in results]
    return results

def go_enrichment(file_path, go_relations_path, gobp, reference_set, go_relations_tree):
    data = pd.read_table(file_path+'_mRNA_list.txt', sep='\t', header=None, encoding='utf-8')
    module_set = set(data.loc[:, 0].values.tolist())&reference_set
    del data
    go = {}
    for term, val in gobp.items():
        term_set = set(val['associated_genes'])
        if len(module_set&term_set)/len(term_set) >= 0.04:
            a, b, c, d = len(module_set&term_set), len(term_set-module_set), len(module_set-term_set), len((reference_set-module_set)&(reference_set-term_set))
            if (a+b+c+d) != len(reference_set):
                print('go_enrichment -->', file_path, term, a, b, c, d, len(reference_set))
            go.update({term:{'Pvalue':pvalue(a, b, c, d).right_tail, 'Genes involved in the term':a, '% Associated genes':(len(module_set&term_set)/len(term_set))*100}})
    sorted_go = sorted(go.items(), key=lambda x: x[1]['Pvalue'])
    del go
    p = [temp[1]['Pvalue'] for temp in sorted_go]
    rank_pvalue = list(ranking.Ranking(p, start=1, reverse=True))
    max_rank = rank_pvalue[-1][0]
    sub_go = {}
    fname = file_path+'_go.txt'
    with open(fname, 'w', encoding='utf-8') as fw:
        fw.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\n'%('GO Term id', 'GO Term name', 'Level', 'Genes involved in the term', '% Associated genes', 'P-value', 'Q-value'))
        for idx in list(range(len(rank_pvalue))):
            pval = rank_pvalue[idx][1]
            qval = rank_pvalue[idx][1]*(float(max_rank)/float(rank_pvalue[idx][0]))
            if qval < 0.05:
                sub_go.update({sorted_go[idx][0]:{'name':gobp[sorted_go[idx][0]]['name'], 'Genes involved in the term':sorted_go[idx][1]['Genes involved in the term'], '% Associated genes':sorted_go[idx][1]['% Associated genes'], 'Q-value':qval}})
                fw.write('%s\t%s\t%s\t%d\t%f\t%e\t%e\n'%(sorted_go[idx][0], gobp[sorted_go[idx][0]]['name'], gobp[sorted_go[idx][0]]['level'], sorted_go[idx][1]['Genes involved in the term'], sorted_go[idx][1]['% Associated genes'], pval, qval))
    go_rel = go_relations_tree.subgraph(list(sub_go.keys()))
    fname = go_relations_path+'_go_relation.txt'
    with open(fname, 'w', encoding='utf-8') as fw:
        fw.write('%s\t%s\t%s\t%s\t%s\t%s\n'%('level', 'GO Term id', 'GO Term name', 'Genes involved in the term', '% Associated genes', 'Q-value'))
        root = {node:sub_go[node]['Q-value'] for (node, in_degree) in go_rel.in_degree() if in_degree == 0}
        for (r, qval) in sorted(root.items(), key=lambda x: x[1]):
            #fw.write('%d\t%s\t%s\t%d\t%f\t%e\n'%(1, r, sub_go[r]['name'], sub_go[r]['Genes involved in the term'], sub_go[r]['% Associated genes'], sub_go[r]['Q-value']))
            spl_dict = nx.single_source_shortest_path_length(nx.dfs_tree(go_rel, r), r)
            if len(spl_dict) > 1:
                fw.write('%s\n'%('\n'.join(['%d\t%s\t%s\t%d\t%f\t%e'%(spl_dict[node]+1, node, sub_go[node]['name'], sub_go[node]['Genes involved in the term'], sub_go[node]['% Associated genes'], sub_go[node]['Q-value']) for node in nx.dfs_preorder_nodes(go_rel, r)])))
    return 0

def gene_ontology_enrichment_analysis(processes, folder_path, case_id, n_subtype, n_module, gene_ontology_file_path, gene_ontology_relations_file_path):
    pool = mp.Pool(processes=processes)
    results = []
    gobp = {}
    reference_set = []
    with open(gene_ontology_file_path, 'r', encoding='utf-8') as fr:
        next(fr)
        for line in fr:
            items = line.strip().split('\t')
            associated_genes = items[3].split('|')
            if len(associated_genes) > 2:
                gobp.update({items[0]:{'name':items[1], 'level':items[2], 'associated_genes':[gene.split(':')[1] for gene in associated_genes]}})
                reference_set.extend(gobp[items[0]]['associated_genes'])
                reference_set = list(set(reference_set))
    tree = pd.read_table(gene_ontology_relations_file_path, header=None, index_col=None, encoding='utf-8')
    go_relations_tree = nx.from_pandas_edgelist(tree, source=tree.columns[0], target=tree.columns[1], create_using=nx.DiGraph())
    go_relations_folder_path = os.path.join(folder_path,'go_relation')
    if (not os.path.exists(go_relations_folder_path)):
       os.mkdir(go_relations_folder_path) 
    if n_subtype > 0:
        for [subtype_idx, n] in n_module:
            for module_idx in range(n):
                go_relations_subtype_folder_path = os.path.join(go_relations_folder_path,'subtype-'+str(subtype_idx))
                if (not os.path.exists(go_relations_subtype_folder_path)):
                   os.mkdir(go_relations_subtype_folder_path) 
                file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), 'lncRNA_association_network_module'+str(module_idx)])])
                go_relations_subtype_path = '/'.join([go_relations_subtype_folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), 'lncRNA_association_network_module'+str(module_idx)])])              
                results.append(pool.apply_async(go_enrichment, args=(file_path, go_relations_subtype_path, gobp, set(reference_set), go_relations_tree)))
    else:
        for module_idx in range(n_module[0][1]):
            file_path = '/'.join([folder_path, '_'.join([case_id, 'lncRNA_association_network_module'+str(module_idx)])])
            go_relations_path =  '/'.join([go_relations_folder_path, '_'.join([case_id, 'lncRNA_association_network_module'+str(module_idx)])])
            results.append(pool.apply_async(go_enrichment, args=(file_path, go_relations_path, gobp, set(reference_set), go_relations_tree)))
    results = [p.get() for p in results]

def heatmap(file_path):
    sns.set(style='white', rc={'axes.edgecolor': '.2', 'axes.linewidth':1.5})
    fontpath = '/usr/share/fonts/truetype/msttcorefonts/Verdana.ttf'
    Fontprop = {'legend':fm.FontProperties(fname=fontpath, size=12), 
                'ticklabels':fm.FontProperties(fname=fontpath, size=10), 
                'label':fm.FontProperties(fname=fontpath, size=12), 
                'title':fm.FontProperties(fname=fontpath, size=14), 
                'cbar_ticklabels':fm.FontProperties(fname=fontpath, size=8)}
    DPI = 600
    CMAP = 'Spectral_r'
    xTickLabels = ['','','','','5','','','','','10']
    yTickLabels = ['10','','','','','5','','','','']
    
    Method = ['Jaccard', 'Simpson', 'Geometric', 'Cosine', 'PCC']
    for method in Method:
        fig = plt.figure(figsize=(6, 5))
        ax = fig.add_subplot(111)
        cbar_ax = fig.add_axes([0.87, 0.25, 0.025, 0.5])
        fname = '_'.join([file_path, method])+'.txt'
        M = pd.read_table(fname, sep='\t', header=None, index_col=None)
        MAX_value = M.max().max()
        hm = sns.heatmap(M, vmin=0, vmax=MAX_value, cmap=CMAP, linewidths=.2, 
                         linecolor='white', cbar=True, square=True, ax=ax, 
                         xticklabels=xTickLabels, yticklabels=yTickLabels, 
                         cbar_ax=cbar_ax)
        plt.setp(cbar_ax.get_yticklabels(), fontproperties=Fontprop['cbar_ticklabels'])
        for item in hm.get_yticklabels():
            item.set_rotation(0)
        plt.setp(ax.get_xticklabels(), horizontalalignment='center', 
                 verticalalignment='top', fontproperties=Fontprop['ticklabels'])
        plt.setp(ax.get_yticklabels(), horizontalalignment='right', 
                 verticalalignment='center', fontproperties=Fontprop['ticklabels'])
        ax.set_ylabel('Similarity according to\nmiRNA', fontproperties=Fontprop['label'])
        ax.set_xlabel('Similarity according to mRNA', fontproperties=Fontprop['label'])
        fname = '_'.join([file_path, method, 'heatmap.png'])
        fig.savefig(fname, format='png', dpi=DPI)
        fig.show(False)
        plt.close()

def graph_association_index_histmatrix(folder_path, case_id, n_subtype):
    if n_subtype > 0:
        for subtype_idx in range(1, n_subtype+1):
            file_path = file_path = '/'.join([folder_path, '_'.join([case_id, 'subtype-'+str(subtype_idx), 'lncRNA_association_index_histmatrix'])])
            heatmap(file_path)
    else:
        file_path = file_path = '/'.join([folder_path, '_'.join([case_id, 'lncRNA_association_index_histmatrix'])])
        heatmap(file_path)

def main(cmdArgument):
    CASE_ID = cmdArgument[1]
    PATH = cmdArgument[2]
    UNIPROT_IDMAPPING_FILE_PATH = '/home/dennistsai/uploads/HUMAN_9606_idmapping_selected.tab'
    GENE_ONTOLOGY_FILE_PATH = '/home/dennistsai/uploads/Gene_Ontology_BiologicalProcess_UniProt-GOA-experimental-evidence-code_20180320.txt'
    GENE_ONTOLOGY_RELATIONS_FILE_PATH = '/home/dennistsai/uploads/go-basic_20180318_tree.txt'
    Num_of_PROCESSES = 3
    if os.path.exists('/'.join([PATH, 'subtype'])):
        Num_of_SUBTYPE = len(os.listdir('/'.join([PATH, 'subtype'])))
        cancer_subtype(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE)
    else:
        Num_of_SUBTYPE = 0
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> start")
    calculate_spearman_rank_order_correlation_coefficient(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE, np.log2(0.7))
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> start bipartite")
    construct_bipartite_coexpression_network(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE)
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> start association indices")
    calculate_association_index(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE)
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> start coherent association")
    Num_of_MODULES = identify_coherent_association(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE, 0.1, UNIPROT_IDMAPPING_FILE_PATH)
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> start gene ontology")
    Num_of_PROCESSES = 5
    gene_ontology_enrichment_analysis(Num_of_PROCESSES, PATH, CASE_ID, Num_of_SUBTYPE, Num_of_MODULES, GENE_ONTOLOGY_FILE_PATH, GENE_ONTOLOGY_RELATIONS_FILE_PATH)
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> start histmatrix")
    graph_association_index_histmatrix(PATH, CASE_ID, Num_of_SUBTYPE)
    print (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + "-> end")
if __name__ == '__main__':
    main(sys.argv) 
