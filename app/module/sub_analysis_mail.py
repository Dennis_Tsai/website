from flask import Flask
import subprocess
import os,sys
from flask.ext.mail import Message, Mail
from subprocess import Popen,PIPE

mail = Mail()
app = Flask(__name__,static_folder='static', static_url_path='')
#app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024i

app.secret_key = 'development key'
app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'st504132005@gm.ym.edu.tw'
app.config["MAIL_PASSWORD"] = 'Dennis13'
mail_sender = 'st504132005@gm.ym.edu.tw'

mail.init_app(app)

def run_analysis(Case_ID,USER_FOLDER):
    print("start analysis")
    case = subprocess.Popen(['python3','/var/www/helloworldapp/app/module/Analysis_code.py',Case_ID,USER_FOLDER], stdout=PIPE, stderr=PIPE, stdin=PIPE) 
    case.wait()    
    return 0
def send_mail(Case_ID,email):
    ctx = app.app_context()
    ctx.push()
    print("start mail")
    mail_subject = "Analysis Finish"
    mail_message = """Dear user: Your analysis is done.\n Please use your Case ID to retrieve your result.\ncase_ID : """+Case_ID
    msg = Message(mail_subject, sender='st504132005@gm.ym.edu.tw', recipients=[email])
    msg.body =  mail_message
    mail.send(msg)
    return 0


def main(cmdArgument):
    CASE_ID = cmdArgument[1]
    PATH = cmdArgument[2]
    email = cmdArgument[3]
    print("start")
    run_analysis(CASE_ID, PATH)
    print("end analysis")
    send_mail(CASE_ID,email)
    print("end mail")
if __name__ == '__main__':
    main(sys.argv) 
