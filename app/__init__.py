from flask import Flask,render_template,redirect, url_for,request,redirect,jsonify,Response,send_from_directory,send_file,make_response,flash
from flask.ext.mail import Message, Mail
from flask_wtf import FlaskForm 
from app import forms
import os, sys, time,base64
import subprocess
from subprocess import Popen,PIPE
from pymongo import MongoClient
from werkzeug import secure_filename
import numpy
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from app.module import change_network_percentage 
from flask_table import Table, Col,create_table

mail = Mail()

app = Flask(__name__,static_folder='static', static_url_path='')
#app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024i

app.secret_key = 'development key'
app.config["MAIL_SERVER"] = "smtp.gmail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'st504132005@gm.ym.edu.tw'
app.config["MAIL_PASSWORD"] = 'Dennis13'
mail_sender = 'st504132005@gm.ym.edu.tw'

mail.init_app(app)

@app.route('/')
def showRoot():
    return render_template('index.html')

ALLOWED_EXTENSIONS = set(['txt', 'docx'])
def allowed_file(filename):

    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

#analysis page

@app.route('/analysis/')
def analysis():
     form = forms.ContactForm()
     return render_template('analysis.html',form=form)

@app.route('/upload_page', methods=['GET','POST'])
def request_page():    
    UPLOAD_FOLDER = '/home/dennistsai/user_upload'
    Case_ID = str(int(time.time()))
    USER_FOLDER = os.path.join(UPLOAD_FOLDER + '/' + Case_ID)
    logfile = USER_FOLDER + 'logfile.txt'
    #if (not os.path.exists(USER_FOLDER)):
    #   os.mkdir(USER_FOLDER)
    #app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    
    ex = request.values.get("example_input")
    user_email = request.values.get("user_email")
    
    if ex == 'on': #use example_file 
        example_type = request.form['example_type']
        if (example_type == "BRCA (with subtype)"):
           Case_ID = "1522597100"
        elif (example_type == "KIRC (without subtype)"):
           Case_ID = "1521530684"
        return  url_for('result',case_id=Case_ID) 
    else:      
        upload_files= ['mRNA','lncRNA','miRNA']
        for f in upload_files:
            file = request.files[f]
            if file and allowed_file(file.filename):
                if (not os.path.exists(USER_FOLDER)):
                    os.mkdir(USER_FOLDER)
                filename = secure_filename(file.filename)
                sub_number = request.form.get("sub_number")
                if str(sub_number) != 'None':
                   USER_SUBTYPE_FOLDER = os.path.join(USER_FOLDER , 'subtype'  )
                   if (not os.path.exists(USER_SUBTYPE_FOLDER)):
                       os.mkdir(USER_SUBTYPE_FOLDER)
                   for x in range(int(sub_number)):
                       fp = open(os.path.join(USER_SUBTYPE_FOLDER , Case_ID + '_subtype_' + str(x+1)+".txt"), "w")
                       subtype_text = request.form.get("subtype_area_"+str(x))
                       for text in subtype_text:
                           if text == "":
                              fp.write("done")
                              break
                           else:
                              fp.write(text)
                       fp.close()
                if f == 'mRNA':
                    file.save(os.path.join(USER_FOLDER, Case_ID + '_mRNA_expression_profile.txt'))
                elif f == 'lncRNA':
                    file.save(os.path.join(USER_FOLDER, Case_ID + '_lncRNA_expression_profile.txt'))
                else:
                    file.save(os.path.join(USER_FOLDER, Case_ID + '_miRNA_expression_profile.txt'))
            else:
                return str('Your file type is not acceptable!!!')
           
        try:
            process = subprocess.Popen(['python3','/var/www/helloworldapp/app/module/sub_analysis_mail.py',Case_ID,USER_FOLDER,user_email], stdout=PIPE, stderr=PIPE, stdin=PIPE)
        except Exception as e:
            return str(e)
           
    return url_for('result', case_id=Case_ID)


@app.route('/result/<case_id>')
def result(case_id):

    return render_template('result.html',case_id=case_id )

#check_result page function
def data_with_subtype(find_ID,USER_FOLDER): #data with subtype
    USER_MODULE_FOLDER = os.path.join(USER_FOLDER,'go_relation')
    USER_SUBTYPE_FOLDER = os.path.join(USER_FOLDER,'subtype')
    num_of_subtype = len(os.listdir(USER_SUBTYPE_FOLDER))
    subtype_list = []
    for i in range(num_of_subtype):
       Cosine_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Cosine_heatmap.png')
       Geometric_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Geometric_heatmap.png')
       Jaccard_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Jaccard_heatmap.png')
       PCC_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_PCC_heatmap.png')
       Simpson_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Simpson_heatmap.png')

       try:
           subprocess.call(['cp',Cosine_img_path,'/var/www/helloworldapp/app/static/user_img'] )
           subprocess.call(['cp',Geometric_img_path,'/var/www/helloworldapp/app/static/user_img'] )
           subprocess.call(['cp',Jaccard_img_path,'/var/www/helloworldapp/app/static/user_img'] )
           subprocess.call(['cp',PCC_img_path,'/var/www/helloworldapp/app/static/user_img'] )
           subprocess.call(['cp',Simpson_img_path,'/var/www/helloworldapp/app/static/user_img'] )
       except Exception as e:
           logfile = open("/home/dennistsai/logfile.txt",'w')
           logfile.write(e)
           return str(e)
       filename = find_ID +  '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Cosine_heatmap.png'
       Cosine_url = url_for('static',filename='user_img/'+filename)
       filename = find_ID +  '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Geometric_heatmap.png'
       Geometric_url = url_for('static',filename='user_img/'+filename)
       filename = find_ID +  '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Jaccard_heatmap.png'
       Jaccard_url = url_for('static',filename='user_img/'+filename)
       filename = find_ID +  '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_PCC_heatmap.png'
       PCC_url = url_for('static',filename='user_img/'+filename)
       filename = find_ID +  '_subtype-' + str(i+1) + '_lncRNA_association_index_histmatrix_Simpson_heatmap.png'
       Simpson_url = url_for('static',filename='user_img/'+filename)

       network_path = USER_FOLDER + '/' + find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_network.txt'
       network = open( network_path ,'r')
       a = []
       nodes = []
       for line in network:
           a = line.split()
           nodes.append(a)
       node_color = USER_FOLDER + '/' + find_ID + '_subtype-' + str(i+1) + '_lncRNA_association_network_nodes_color.txt'
       color = open( node_color ,'r')
       b = []
       color_pairs = []
       for line in color:
        b = line.split()
        color_pairs.append(b)

       module_list = []
       USER_SUBTYPE_MODULE = os.path.join(USER_MODULE_FOLDER,'subtype-'+str(i+1))
       num_of_module = len(os.listdir(USER_SUBTYPE_MODULE))
       for j in range(num_of_module):
        go_relation = pd.read_table(os.path.join(USER_SUBTYPE_MODULE,find_ID+'_subtype-'+str(i+1)+'_lncRNA_association_network_module'+str(j)+"_go_relation.txt"), sep='\t')
        module = go_relation.to_html(index=False)
        module_list.append(module)
       
       subtype = "true" 
       subtype_list.append( (Cosine_url,Geometric_url,Jaccard_url,PCC_url,Simpson_url,find_ID,nodes,color_pairs,module_list,subtype) )
    return subtype_list
    
def data_with_no_subtype(find_ID,USER_FOLDER): #data with no subtype
    data_list = []
    USER_MODULE_FOLDER = os.path.join(USER_FOLDER,'go_relation')
    num_of_module = len(os.listdir(USER_MODULE_FOLDER))

    Cosine_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID+'_lncRNA_association_index_histmatrix_Cosine_heatmap.png')
    Geometric_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID+'_lncRNA_association_index_histmatrix_Geometric_heatmap.png')
    Jaccard_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID+'_lncRNA_association_index_histmatrix_Jaccard_heatmap.png')
    PCC_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID+'_lncRNA_association_index_histmatrix_PCC_heatmap.png')
    Simpson_img_path = os.path.join('/home/dennistsai/user_upload',find_ID,find_ID+'_lncRNA_association_index_histmatrix_Simpson_heatmap.png')

    try:
       subprocess.call(['cp',Cosine_img_path,'/var/www/helloworldapp/app/static/user_img'] )
       subprocess.call(['cp',Geometric_img_path,'/var/www/helloworldapp/app/static/user_img'] )
       subprocess.call(['cp',Jaccard_img_path,'/var/www/helloworldapp/app/static/user_img'] )
       subprocess.call(['cp',PCC_img_path,'/var/www/helloworldapp/app/static/user_img'] )
       subprocess.call(['cp',Simpson_img_path,'/var/www/helloworldapp/app/static/user_img'] )
    except Exception as e:
       logfile = open("/home/dennistsai/logfile.txt",'w')
       logfile.write(e)
       return str(e)
    filename = find_ID + '_lncRNA_association_index_histmatrix_Cosine_heatmap.png'
    Cosine_url = url_for('static',filename='user_img/'+filename)
    filename = find_ID + '_lncRNA_association_index_histmatrix_Geometric_heatmap.png'
    Geometric_url = url_for('static',filename='user_img/'+filename)
    filename = find_ID + '_lncRNA_association_index_histmatrix_Jaccard_heatmap.png'
    Jaccard_url = url_for('static',filename='user_img/'+filename)
    filename = find_ID + '_lncRNA_association_index_histmatrix_PCC_heatmap.png'
    PCC_url = url_for('static',filename='user_img/'+filename)
    filename = find_ID + '_lncRNA_association_index_histmatrix_Simpson_heatmap.png'
    Simpson_url = url_for('static',filename='user_img/'+filename)

    network_path = USER_FOLDER + '/' + find_ID + '_lncRNA_association_network.txt'
    network = open( network_path ,'r')
    a = []
    nodes = []
    for line in network:
        a = line.split()
        nodes.append(a)
    node_color = USER_FOLDER + '/' + find_ID + '_lncRNA_association_network_nodes_color.txt'
    color = open( node_color ,'r')
    b = []
    color_pairs = []
    for line in color:
        b = line.split()
        color_pairs.append(b)
        color_json = jsonify(color_pairs)

    module_list = []
    for i in range(num_of_module):
        go_relation = pd.read_table(os.path.join(USER_MODULE_FOLDER,find_ID+'_lncRNA_association_network_module'+str(i)+"_go_relation.txt"), sep='\t')
        module = go_relation.to_html(index=False)
        module_list.append(module)
    subtype = 'false'
    data_list.append((Cosine_url,Geometric_url,Jaccard_url,PCC_url,Simpson_url,find_ID,nodes,color_pairs,module_list,subtype))
    return data_list

#check result page
@app.route('/check_result/')
def check_result():
       return render_template('check_result.html')
    

@app.route('/send_data', methods=['GET','POST'])
def send_data():
    find_ID = request.form['find_ID']
    UPLOAD_FOLDER = '/home/dennistsai/user_upload'
    USER_FOLDER = os.path.join(UPLOAD_FOLDER + '/' + find_ID)
    USER_SUBTYPE_FOLDER = os.path.join(USER_FOLDER , 'subtype'  )
    if (not os.path.exists(USER_FOLDER)):
       return str("Your case is not exist!!!")
    else:
       if os.path.exists(USER_SUBTYPE_FOLDER): #with subtype
          return jsonify( data_with_subtype(find_ID,USER_FOLDER) )
       else:  #Nosubtype
          data_list = data_with_no_subtype(find_ID,USER_FOLDER)
          return jsonify( data_list)
    
#Guide Book
@app.route('/guide_book/')
def guide_book():
     return render_template('guide_book.html')

#Contact US
@app.route('/contact_us/', methods=['GET', 'POST'])
def contact_us():
    form = forms.ContactForm()
    if request.method == 'POST':
       if form.validate() == False:
          flash('All fields are required.')
          return render_template('contact_us.html', form=form)
       else:
          msg = Message(form.subject.data, sender='st504132005@gm.ym.edu.tw', recipients=['st504132005@gmail.com'])
          msg.body = """
          From: %s <%s>
          %s
          """ % (form.name.data, form.email.data, form.message.data)
          mail.send(msg)
          
          return render_template('contact_us.html', success=True)
    elif request.method == 'GET':
        return render_template('contact_us.html', form=form) 
    #return render_template('contact_us.html')
@app.route('/table/')
def table():
    return render_template("GO_table.html")
@app.route('/test',methods=['GET','POST'])
def test():
    num = request.form['find_ID']
    return str(num)

@app.route('/ex',methods=['GET','POST'])
def ex():
    cutoff = request.form['percentage_text']
    find_ID = request.form['case_ID']
    UPLOAD_FOLDER = '/home/dennistsai/user_upload'
    USER_FOLDER = os.path.join(UPLOAD_FOLDER + '/' + find_ID)
 

    UNIPROT_IDMAPPING_FILE_PATH = '/home/dennistsai/uploads/HUMAN_9606_idmapping_selected.tab'
    
    process = subprocess.Popen(['python3','/var/www/helloworldapp/app/module/change_network_percentage.py',find_ID,USER_FOLDER,cutoff], stdout=PIPE, stderr=PIPE, stdin=PIPE)
    process.wait()
    #change_network_percentage.identify_coherent_association(Num_of_PROCESSES, PATH, find_ID, Num_of_SUBTYPE, percentage, UNIPROT_IDMAPPING_FILE_PATH)

    network_path = USER_FOLDER + '/' + find_ID + '_lncRNA_association_network_modified.txt'
    network = open( network_path ,'r')
    a = []
    nodes = []
    for line in network:
        a = line.split()
        nodes.append(a)
    node_color = USER_FOLDER + '/' + find_ID + '_lncRNA_association_network_nodes_color_modified.txt'
    color = open( node_color ,'r')
    b = []
    color_pairs = []
    for line in color:
        b = line.split()
        color_pairs.append(b)
        color_json = jsonify(color_pairs)
    data_list = []
    data_list.append((nodes, color_pairs)) 
    return jsonify(data_list)

@app.route('/example_data/')
def example_data():
    return render_template("example_data.html")

@app.route('/example_type',methods=['GET','POST'])
def example_type():
    cancer_type = request.form['example_type']
    if cancer_type == "BRCA" :
        CASE_FOLDER = "/home/dennistsai/website_example/BRCA"
        CASE_ID = "1522597100"
        return jsonify( data_with_subtype(CASE_ID,CASE_FOLDER))
    elif cancer_type == "KIRC" :
        CASE_FOLDER = "/home/dennistsai/website_example/KIRC"
        CASE_ID = "1521530684"
        data_list = data_with_no_subtype(CASE_ID,CASE_FOLDER)
        return jsonify( data_list )
    elif cancer_type == "BLCA" :
        CASE_FOLDER = "/home/dennistsai/website_example/BLCA"
        CASE_ID = "1526019585"
        data_list = data_with_no_subtype(CASE_ID,CASE_FOLDER)
        return jsonify( data_list )
    elif cancer_type == "COAD" :
        CASE_FOLDER = "/home/dennistsai/website_example/COAD"
        CASE_ID = "1526020647"
        data_list = data_with_no_subtype(CASE_ID,CASE_FOLDER)
        return jsonify( data_list )
    elif cancer_type == "LUSC" :
        CASE_FOLDER = "/home/dennistsai/website_example/LUSC"
        CASE_ID = "1526025122"
        data_list = data_with_no_subtype(CASE_ID,CASE_FOLDER)
        return jsonify( data_list )
    elif cancer_type == "OV" :
        CASE_FOLDER = "/home/dennistsai/website_example/OV"
        CASE_ID = "1526051491"
        data_list = data_with_no_subtype(CASE_ID,CASE_FOLDER)
        return jsonify( data_list )


    
if __name__ == '__main__':
    #app.debug = True
    app.run(debug=True())
